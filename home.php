<?php
//session_start();
require_once 'home_html.php';

function procesiraj_upload(){

    $error = "";
    //ukoliko korisnik jos nije dodavao svoje slike stvori njegov folder
    if ( !file_exists('Image/'.$_SESSION['user']) ) {
        mkdir( 'Image/'.$_SESSION['user'], 0777, true );
    }

    $nameOfDir = "Image/". $_SESSION['user'] ."/";
    $nameOfImg = $nameOfDir . basename( $_FILES["fileToUpload"]["name"] );
    $uploadOk = 1;

    $imageFileType = pathinfo( $nameOfImg, PATHINFO_EXTENSION );

    // Check if image file is a actual image or fake image
    $check = getimagesize( $_FILES["fileToUpload"]["tmp_name"] );
    if($check !== false) {
        $error .= "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    }
    else {
        $error .= "File nije slika. ";
        $uploadOk = 0;
    }

    // Check if file already exists
    if ( file_exists( $nameOfImg ) ) {
        $error .= "Slika s tim imenom već postoji. ";
        $uploadOk = 0;
    }
    // velicina slike
    if ( $_FILES["fileToUpload"]["size"] > 500000 ) {
        $error .= "Slika je prevelika (>500Kb). ";
        $uploadOk = 0;
    }

    $imageFileType = strtolower($imageFileType);
    // odredjeni formati
    if( $imageFileType != "jpg" && $imageFileType != "png" &&
        $imageFileType != "jpeg" && $imageFileType != "gif" ) {
        $error = "Dopušteni formati su JPG, JPEG, PNG & GIF. ";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ( $uploadOk == 0 ) {
        crtaj_home( $error. "Slika nije spremljena." );
        return;
    }
    else {
        if ( move_uploaded_file( $_FILES["fileToUpload"]["tmp_name"], $nameOfImg ) ) {
            crtaj_home( "Slika je upload-ana." );
            return;
        }
        else {
            crtaj_home( "Došlo je do problema prilikom upload-anja slike." );
            return;
        }
    }
}

//logout
function procesiraj_logout(){
    session_unset();
    session_destroy();
    header("Location: index.php");
}

//igra
function procesiraj_game(){
    $var = $_POST["level"]."/".$_POST["slike"];
    crtaj_memoryGame( $var );
}


//glavni dio
if( isset( $_POST["gumb" ] ) && $_POST["gumb"] === "upload" )
    procesiraj_upload();
else if( isset( $_POST["gumb" ] ) && $_POST["gumb"] === "logout" )
    procesiraj_logout();
else if( isset( $_POST["gumb" ] ) && $_POST["gumb"] === "play" )
    procesiraj_game();
else if( isset( $_POST["gumb" ] ) && $_POST["gumb"] === "goback" )
    crtaj_home();
else
    crtaj_home();

?>
