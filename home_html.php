<?php
session_start();
function crtaj_home( $message = '' ){
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Home</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/custom.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container-fluid">
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">
        MemoryGame
      </a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-right">
        <li> <p class="navbar-text navbar-left">Dobrodošao/la <?php echo $_SESSION['name'];?> </p> </li>
        <li>
          <form method="POST" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">
          <button type="submit" class="btn btn-default navbar-btn"  name="gumb" value="logout"><span class="glyphicon glyphicon-log-out"></span></button>
          </form>
      </li>
      </ul>
    </div> <!-- zatvara collapse -->

</nav>
</div>


<div class="container">
  <div class="row">
    <div class="col-lg-4">
        <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post" enctype="multipart/form-data">
        Dodajte svoje slike za igru:
        <input type="file" name="fileToUpload" id="file">
        <span class="file-custom"></span> <br />
        <button type="submit" class="btn btn-default" name="gumb" value="upload">Dodajte sliku</button>
        </form>
        <?php
        if( $message !== '' )
          echo '<p id="error">' . $message . '</p>';
        ?>
    </div>
    <div class="col-lg-2"></div>
    <div class="col-lg-6">
        <form method="POST" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">
          Odaberite razinu: <br >
        <select class="form-control input-lg" name="level">
          <option value="1">Lagano</option>
          <option value="2">Srednje</option>
          <option value="3">Teško</option>
        </select>
        <br >
          Želite li igrati sa slikama iz svog foldera? <br />
        <select class="form-control input-lg" name="slike">
          <option value="0">Da</option>
          <option value="1">Ne</option>
        </select>
        <br>
        <button type="submit" class="btn btn-danger" id="playGame" name="gumb" value="play">Provjeri pamćenje!</button>
        </form>
    </div>
  </div>
</div>

</body>
<script type="text/javascript">
$(document).ready(function(){

function checkErrorMsg(){
  if( $("#error").html()!= "" )
    $("#error").html("");
}

setInterval(checkErrorMsg, 2000);
});

</script>

</html>

<?php
}

function crtaj_memoryGame( $v ){
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>YouPlayGame</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/custom.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="memory.js"></script>

  <style type="text/css">

  #imageDiv { padding: 0;  margin: 0;}
  #game {
  padding: 0;
  float: left;
  margin: 10px;
  list-style: none;
  outline: inset lightblue;
  width: 100px;
  height: 100px;
  }

  img {
  width: 100px;
  height: 100px;
  }

</style>

</head>
<body>
<div class="container-fluid">
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">
        MemoryGame
      </a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-right">
        <li> <p class="navbar-text navbar-left">Dobrodošao/la <?php echo $_SESSION['name'];?> </p> </li>
        <li>
          <form method="POST" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">
          <button type="submit" class="btn btn-default navbar-btn"  name="gumb" value="logout"><span class="glyphicon glyphicon-log-out"></span></button>
          </form>
      </li>
      </ul>
    </div> <!-- zatvara collapse -->

</nav>
</div>
<div class="jumbotron">
<form method="POST" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">
    <button type="submit" class="btn btn-danger" name="gumb" value="goback">Vrati se na početnu stranicu</button>
</form>
</div>

<div class="container">

  <p id="hid"><?php echo $v."/".$_SESSION['user'];?></p> <br > <br >
  <p id="jedan"></p>

  <ul id="imageDiv">
  </ul>

</div>

<!--
<form method="POST" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">
    <button type="submit" class="btn btn-danger" name="gumb" value="goback">Vrati se na početnu stranicu</button>
</form>
-->

</body>

<script type="text/javascript">

$(document).ready(function(){

  $("#hid").hide();
  var par = $("#hid").html();
  var arr = par.split("/");
  //$("#jedan").html("proba " + arr[0] + " " + arr[1]);
  $.ajax({

    url: "getImages.php",
    data:
    {
      level: arr[0],
      images: arr[1],
      user: arr[2]
    },
    dataType: "json",
    success: function (data) {

      if ( typeof data === "string" ){
          $("#jedan").html( data );
      }
      else{
      for(var i = 0; i < data.length; ++i) {

          var insert = "<li id=\"game\"><img src=\""+ data[i] +"\"></li>";
          $('#imageDiv').append(insert);
          $("img").hide();
      }
    }

    }

  });

});

</script>
</html>
<?php
}
?>
